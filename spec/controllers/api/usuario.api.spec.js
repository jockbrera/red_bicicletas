const request = require('request');
const server = require('../../../bin/www');
const helperDBConnection = require('../../helpers/mongoDBConnection');
const Bicicleta = require('../../../models/bicicleta');
const Usuario = require('../../../models/usuario');

describe('Bicicleta API', function() {
    // this creates and closes the connection in a beforeAll and afterAll methods
    helperDBConnection();
    
    describe('GET - list usuarios /', function() {
        it('should return 200', function(done) {
            request.get('http://localhost:3000/api/usuarios', function(error, response, body) {
                var parsedBody = JSON.parse(body);
            
                expect(response.statusCode).toBe(200);
                expect(parsedBody.usuarios.length).toBe(0);
                
                done();
            });
        });
    });

    describe('POST - add usuario /create', function() {
        it('should return 201', function(done) {
            var usuario = {
                nombre: 'Laura'
            };

            request.post({
                headers: {'content-type': 'application/json'},
                url: 'http://localhost:3000/api/usuarios/create',
                body: JSON.stringify(usuario)
            }, function(error, response, body) {
                var parsedBody = JSON.parse(body);
                
                expect(response.statusCode).toBe(201);
                expect(parsedBody.nombre).toBe(usuario.nombre);

                Usuario.getAllUsuarios().then(function(usuarios) {
                    expect(usuarios.length).toBe(1);

                    done();
                });
            });
        });
    });

    describe('POST - create a reservation /reserve', function() {
        it('should return 201', function(done) {
            var usuario = Usuario.createInstance('Marcos');
            var bici = Bicicleta.createInstance(1, 'rojo', 'raptor', [-31.4158007,-64.1825873]);
            var reserva = {
                    "desde": "2020-11-01",
                    "hasta": "2020-11-03",
                    "biciId": bici._id
                };
            
            // save in mongoDB user and bici
            usuario.save();
            bici.save();

           request.post({
                headers: {'content-type': 'application/json'},
                url: `http://localhost:3000/api/usuarios/${usuario._id}/reserve`,
                body: JSON.stringify(reserva)
            }, function(error, response, body) {
                var parsedBody = JSON.parse(body);

                expect(response.statusCode).toBe(201);
                expect(parsedBody.usuario).toEqual(usuario._id.toString());
                expect(parsedBody.bicicleta).toEqual(bici._id.toString());

                done();
            });
        });
    });

    describe('PUT - update usuario /:id/update', function() {
        it('should return 200', function(done) {
            var usuario = Usuario.createInstance('Maria');
            var usuarioUpdated = {
                nombre: 'Analia'
            };
 
            // Add usuario to the list
            Usuario.add(usuario).then(function(newUsuario) {
                expect(newUsuario).toEqual(usuario);

                request.put({
                    headers: {'content-type': 'application/json'},
                    url: `http://localhost:3000/api/usuarios/${newUsuario._id}/update`,
                    body: JSON.stringify(usuarioUpdated)
                }, function(error, response, body) {
                    var parsedBody = JSON.parse(body);
                    
                    expect(response.statusCode).toBe(200);
                    expect(parsedBody.nombre).toBe(usuarioUpdated.nombre);
                    
                    done();
                });
            });
        });
    });

    describe('DELETE - delete usuario /:id/delete', function() {
        it('should return 204', function(done) {
            var usuario = Usuario.createInstance('Maria');

            // Add bicis to the list
            Usuario.add(usuario).then(function(newUsuario) {
                expect(newUsuario).toEqual(usuario);

                request.delete(`http://localhost:3000/api/usuarios/${newUsuario._id}/delete`,
                    function(error, response, body) {
                        expect(response.statusCode).toBe(204);
                        
                        Usuario.getAllUsuarios().then(function(usuarios) {
                            // verify list is empty
                            expect(usuarios.length).toBe(0);
        
                            done();
                        });
                    }
                );
            });
        });
    });
});
