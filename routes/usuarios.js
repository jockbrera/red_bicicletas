var express = require('express');
var router = express.Router();
var usuarioController = require('../controllers/usuario');

// list
router.get('/', usuarioController.list);
// add
router.get('/create', usuarioController.create_get);
router.post('/create', usuarioController.create_post);
// update
router.get('/:id/update', usuarioController.update_get);
router.post('/:id/update', usuarioController.update_post);
// delete
router.post('/:id/delete', usuarioController.delete_post);

module.exports = router;