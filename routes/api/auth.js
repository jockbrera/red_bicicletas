const express = require('express');
const router = express.Router();
const authApiController = require('../../controllers/api/authApi');

router.post('/authenticate', authApiController.authenticate);
router.post('/forgotpassword', authApiController.forgotpassword);

module.exports = router;
