var express = require('express');
var router = express.Router();
var tokenController = require('../controllers/token');

// confirmation
router.get('/confirmation/:token', tokenController.confirmation);

module.exports = router;