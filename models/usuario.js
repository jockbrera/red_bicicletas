const mongoose = require('mongoose');
const bcrypt = require('bcrypt');
const crypto = require('crypto');
const Promise = require('promise');
const uniqueValidator = require('mongoose-unique-validator');
const Reserva = require('./reserva');
const Token = require('./token');
const mailer = require('../mailer/mailer');

const saltRounds = 10;
const emailRx = /^[\w-\.]+@([\w-]+\.)+[\w-]{2,4}$/;
const Schema = mongoose.Schema;

const UsuarioSchema = new Schema({
    nombre: {
        type: String,
        required: [true, 'El nombre es requerido'],
        trim: true,
    },
    email: {
        type: String,
        required: [true, 'El email es requerido'],
        trim: true,
        lowercase: true,
        unique: true,
        validate: {
            validator: function(email) {
                return emailRx.test(email);
            },
            message: 'Por favor ingrese un email válido'
        },
        match: [/^[\w-\.]+@([\w-]+\.)+[\w-]{2,4}$/, 'Por favor ingrese un email valido']
    },
    password: {
        type: String,
        required: [true, 'El password es requerido']
    },
    passwordResetToken: String,
    passwordResetTokenExpires: Date,
    verificado: {
        type: Boolean,
        default: false
    }
});

// Plugins
UsuarioSchema.plugin(uniqueValidator, { message: 'Este {PATH} ya existe con otro usuario' });

// Pre actions
UsuarioSchema.pre('save', function(next) {
    if(this.isModified('password')) {
        this.password = bcrypt.hashSync(this.password, saltRounds);
    }

    next();
});

// Class Methods
UsuarioSchema.statics.createInstance = function(nombre, email, password) {
    return new this({
        nombre: nombre,
        email: email,
        password: password
    });
};

UsuarioSchema.statics.getAllUsuarios = function(callback) {
    return this.find({}, callback);
};

UsuarioSchema.statics.add = function(data, callback) {
    return this.create(data, callback);
};

UsuarioSchema.statics.update = function(id, data, callback) {
    return this.updateOne({ _id: id }, data, callback);
};

UsuarioSchema.statics.findById = function(id, callback) {
    return this.findOne({ _id: id }, callback);
};

UsuarioSchema.statics.deleteById = function(id, callback) {
    return this.findByIdAndDelete(id, callback);
};

UsuarioSchema.statics.findOrCreateByGoogle = function(profile, callback) {
    // save this reference
    var self = this;

    return this.findOne({
        $or: [
            {
                'googleId': profile.id
            },
            {
                'email': profile.emails[0].value
            }
        ]
    }, function(error, usuario) {
        if (error) {
            return callback(error, null);
        } else if (usuario) {
            return callback(null, usuario);
        } else {
            // create a new User with google's data
            let data = {};

            data.googleId = profile.id;
            data.email = profile.emails[0].value;
            data.nombre = profile.displayName || 'SIN NOMBRE';
            data.password = profile.id;
            data.verificado = true;

            console.log('new google user being saved: ', data);

            // create new user in database
            return self.create(data, callback);
        }
    });
};

// Instance Methods
UsuarioSchema.methods.reserve = function(biciId, desde, hasta, callback) {
    var reserva = new Reserva({
        desde: desde,
        hasta: hasta,
        usuario: this._id,
        bicicleta: biciId
    });

    return reserva.save(callback);
};

UsuarioSchema.methods.sendConfirmationEmail = function() {
    const token = { usuario: this.id, token: crypto.randomBytes(16).toString('hex') };
    const destintation_email = this.email;

    console.log(token);

    return Token.add(token).then(function(newToken) {
        console.log('newToken: ' + newToken);

        const mailOptions = {
            from: process.env.EMAIL_SENDER,
            to: destintation_email,
            subject: 'Verificación de cuenta',
            text: 'Hola, \n\n Por favor, para confirmar su cuenta haga click en el siguiente link: \n',
            html: '<a href="http:\\localhost:3000/token/confirmation/' + token.token + '">Confirmar Cuenta</a>'
        };
        const promise = new Promise(function (resolve, reject) {
            mailer.sendMail(mailOptions, function(error) {
                if (error) {
                    console.log(error);
                    reject(error);
                }
    
                console.log('Se ha enviado un email para confirmar la cuenta a: ' + destintation_email + '.');
                resolve();
            });
        });

        return promise;
    }).catch(function(error) {
        console.log(error);
        return error;
    });
};

UsuarioSchema.methods.resetPassword = function() {
    const token = new Token({ usuario: this.id, token: crypto.randomBytes(16).toString('hex') });
    const destintation_email = this.email;

    return token.save().then(function() {
        const mailOptions = {
            from: process.env.EMAIL_SENDER,
            to: destintation_email,
            subject: 'Reseteo de Password de cuenta',
            text: 'Hola, \n\n Por favor, para resetear el password de su cuenta haga click en este link: \n',
            html: '<a href="http:\\localhost:3000/resetpassword/' + token.token + '">Resetear Password</a>'
        };

        const promise = new Promise(function (resolve, reject) {
            mailer.sendMail(mailOptions, function(error) {
                if (error) {
                    console.log(error);
                    reject(error);
                }
    
                console.log('Se ha enviado un email para resetear el password a: ' + destintation_email + '.');
                resolve();
            });
        });

        return promise;
    }).catch(function(error) {
        console.log(error);
        return error;
    });
};

UsuarioSchema.methods.isValidPassword = function(password) {
    return bcrypt.compareSync(password, this.password);
};

module.exports = mongoose.model('Usuario', UsuarioSchema);
