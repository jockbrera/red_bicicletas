var mongoose = require('mongoose');
var Schema = mongoose.Schema;

var TokenSchema = new Schema({
    usuario: {
        type: mongoose.Schema.Types.ObjectId,
        required: true,
        ref: 'Usuario'
    },
    token: {
        type: String,
        required: true
    },
    createdAt: {
        type: Date,
        required: true,
        default: Date.now,
        expires: 300
    }
});

// Class Methods
TokenSchema.statics.createInstance = function(usuario, token, createdAt) {
    return new this({
        usuario: usuario,
        token: token,
        createdAt: createdAt
    });
};

TokenSchema.statics.add = function(data, callback) {
    return this.create(data, callback);
};

TokenSchema.statics.findByTokenAndPopulate = function(token, callback) {
    return this.findOne({ token: token }).populate('usuario').exec(callback);
};

module.exports = mongoose.model('Token', TokenSchema);
;
