const passport = require('passport');
const LocalStrategy = require('passport-local').Strategy;
const GoogleStrategy = require('passport-google-oauth20').Strategy;
const Usuario = require('../models/usuario');

// Local Strategy
passport.use(new LocalStrategy(function(email, password, done) {
    Usuario.findOne({ email: email }).then(function(usuario) {
        if(!usuario) {
            return done(null, false, { message: 'Email inexistente o incorrecto' });
        }
        
        if(!usuario.isValidPassword(password)) {
            return done(null, false, { message: 'Password incorrecto' });
        }

        // User is valid
        return done(null, usuario);
    }).catch(function(error) {
        console.log(error);
        return done(null, false, { message: 'Error validando las credenciales' });
    });
}));

// Use the GoogleStrategy within Passport.
//   Strategies in Passport require a `verify` function, which accept
//   credentials (in this case, an accessToken, refreshToken, and Google
//   profile), and invoke a callback with a user object.
const localHost = process.env.NODE_ENV === 'development' ?
    process.env.LOCAL_HOST + ':' + process.env.PORT :
    process.env.LOCAL_HOST;

passport.use(new GoogleStrategy(
    {
        clientID: process.env.GOOGLE_CLIENT_ID,
        clientSecret: process.env.GOOGLE_CLIENT_SECRET,
        callbackURL: localHost + '/auth/google/callback'
    },
    function(accessToken, refreshToken, profile, done) {
        console.log(profile);
        Usuario.findOrCreateByGoogle(profile, function (error, usuario) {
            if (error) {
                console.log(error);
            }
            return done(error, usuario);
        });
    }
));

passport.serializeUser(function(usuario, callback) {
    callback(null, usuario.id);
});

passport.deserializeUser(function(id, callback) {
    Usuario.findById(id, function(error, usuario) {
        callback(error, usuario);
    });
});

module.exports = passport;