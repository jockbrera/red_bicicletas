// config dotenv to read .env file
dotenv = require('dotenv').config();

const createError = require('http-errors');
const express = require('express');
const path = require('path');
const cookieParser = require('cookie-parser');
const logger = require('morgan');
const mongoose = require('mongoose');

const passport = require('./config/passport');
const session = require('express-session');
const MongoDBStore = require('connect-mongodb-session')(session);
const jwt = require('jsonwebtoken');

const indexRouter = require('./routes/index');
const bicicletasRouter = require('./routes/bicicletas');
const usuariosRouter = require('./routes/usuarios');
const tokenRouter = require('./routes/token');
const bicicletasApiRouter = require('./routes/api/bicicletas');
const usuariosApiRouter = require('./routes/api/usuarios');
const reservasApiRouter = require('./routes/api/reservas');
const authApiRouter = require('./routes/api/auth');

const Usuario = require('./models/usuario');
const Token = require('./models/token');

const app = express();

// set environmental variables
app.set('env', process.env.NODE_ENV);
app.set('jwtsecretkey', process.env.JWT_TOKEN_SECRET_KEY);

// set session's strategy to use to save express' sessions
const mongoDBUri = process.env.MONGODB_URI;
let store = new session.MemoryStore();

if (process.env.NODE_ENV === 'development') {
  store = new session.MemoryStore();
} else {
  store = new MongoDBStore({
    uri: mongoDBUri,
    collection: 'sessions',
  });
  store.on('error', function (error) {
      assert.ifError(error);
      assert.ok(false);
  });
}

// initialize and configure express' session
app.use(session({
  cookie: { maxAge: 24 * 60 * 60 * 1000 },
  store: store,
  saveUninitialized: true,
  resave: true,
  secret: process.env.SESSION_SECRET_KEY
}));

// connect to mongoDB
mongoose.Promise = global.Promise;
// this enables validators on update
mongoose.set('runValidators', true);

mongoose.connect(mongoDBUri, {
  useNewUrlParser: true,
  useUnifiedTopology: true
});
mongoose.connection.on('error', console.error.bind(console, 'MongoDB connection error'));
mongoose.connection.once('open', function () {
  console.log('Connection successfully');
});

// view engine setup
app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'pug');

app.use(logger('dev'));
app.use(express.json());
app.use(express.urlencoded({ extended: false }));
app.use(cookieParser());
app.use(passport.initialize());
app.use(passport.session());
app.use(express.static(path.join(__dirname, 'public')));
app.use('/static', express.static(path.join(__dirname, 'node_modules')));

// login routes
app.get('/login', function(req, res) {
  res.render('session/login');
});

app.post('/login', function(req, res, next) {
  passport.authenticate('local', function(error, usuario, info) {
    if(error) {
      console.log(error);
      return next(error);
    }
    
    if(!usuario) {
      return res.render('session/login', { info });
    }
    
    // establishing a session
    req.logIn(usuario, function(error) {
      if(error) {
        console.log(error);
        return req.next(error);
      }

      return res.redirect('/');
    });
  })(req, res, next);
});

app.get('/logout', function(req, res) {
  req.logOut();
  res.redirect('/');
});

app.get('/forgotpassword', function(req, res) {
  res.render('session/forgotpassword');
});

app.post('/forgotpassword', function(req, res) {
  Usuario.findOne({ email: req.body.email }, function(error, usuario) {
    if(error) {
      console.log(error);
      return res.render('session/forgotpassword', { info: { message: 'Error al validar su email' } });
    }

    if(!usuario) {
      return res.render('session/forgotpassword', { info: { message: 'No se encontró usuario con email indicado' } });
    }
    // send email to restore password
    usuario.resetPassword().then(function() {
      // render informative's page
      res.render('session/forgotPasswordMessage');
    }).catch(function(error) {
      return res.render('session/forgotpassword', { info: { message: 'Falló el envio de email a su cuenta' } });
    });
  });
});

app.get('/resetpassword/:token', function(req, res) {
  Token.findOne({ token: req.params.token }).populate('usuario').exec(function(error, token) {
    if(error) {
      console.log(error);
      return res.render('session/resetpassword', { usuario: {}, errors: {}, info: { message: 'Error validando tu token' } });
    }

    if(!token) {
      return res.render('session/resetpassword', { usuario: {}, errors: {}, info: { message: 'Token inválido o expirado, por favor inicie el proceso de reseteo nuevamente' } });
    }

    res.render('session/resetpassword', { usuario: token.usuario, errors: {}, info: {} });
  });
});

app.post('/resetpassword', function(req, res) {
  const email = req.body.email;
  const password = req.body.password;
  const confirm_password = req.body.confirm_password;

  if(password !== confirm_password) {
    return res.render('session/resetpassword', { usuario: { email: email }, errors: { confirm_password: { message: 'El password de confirmation no es igual al password' } } });
  }

  Usuario.findOne({ email: email }, function(error, usuario) {
    if(error) {
      console.log(error);
      return res.render('session/resetpassword', { usuario: { email: email }, errors: {}, info: { message: 'Error validando su usuario' } });
    }

    if(!usuario) {
      return res.render('session/resetpassword', { usuario: { email: email }, errors: {}, info: { message: 'Usuario no encontrado.' } });
    }

    // set new password
    usuario.password = password;
    // save changes
    usuario.save(function(error) {
      if(error) {
        console.log(error);
        return res.render('session/resetpassword', { usuario: { email: email }, errors: error.errors, info: {} });
      }

      console.log('redirecting to login');
      res.redirect('/login');
    });
  });
});

// GET /auth/google
//   Use passport.authenticate() as route middleware to authenticate the
//   request.  The first step in Google authentication will involve
//   redirecting the user to google.com.  After authorization, Google
//   will redirect the user back to this application at /auth/google/callback
app.get('/auth/google',
  passport.authenticate('google', {
      scope: ['openid',
        'https://www.googleapis.com/auth/userinfo.email',
        'https://www.googleapis.com/auth/userinfo.profile'
      ]
    }
  )
);

// GET /auth/google/callback
//   Use passport.authenticate() as route middleware to authenticate the
//   request.  If authentication fails, the user will be redirected back to the
//   login page.  Otherwise, the primary route function function will be called,
//   which, in this example, will redirect the user to the home page.
app.get('/auth/google/callback',
  passport.authenticate('google', { failureRedirect: '/login' }),
  function(req, res) {
    res.redirect('/');
  }
);

// Web routes
app.use('/', indexRouter);
app.use('/bicicletas', isUserloggedIn, bicicletasRouter);
app.use('/usuarios', isUserloggedIn, usuariosRouter);
app.use('/token', tokenRouter);
// Api routes
app.use('/api/bicicletas', validateUserAccess, bicicletasApiRouter);
app.use('/api/usuarios', usuariosApiRouter);
app.use('/api/reservas', validateUserAccess, reservasApiRouter);
app.use('/api/auth', authApiRouter);

// catch 404 and forward to error handler
app.use(function(req, res, next) {
  next(createError(404));
});

// error handler
app.use(function(err, req, res, next) {
  // set locals, only providing error in development
  res.locals.message = err.message;
  res.locals.error = req.app.get('env') === 'development' ? err : {};

  // render the error page
  res.status(err.status || 500);
  res.render('error');
});

function isUserloggedIn(req, res, next) {
  console.log(req.user);
  if(req.user) {
    next();
  } else {
    console.log('Usuario no logueado');
    res.redirect('/login');
  }
}

function validateUserAccess(req, res, next) {
  jwt.verify(req.headers['x-access-token'], req.app.get('jwtsecretkey'), function(error, tokenDecoded) {
    if(error) {
      console.log(error);
      return res.status(401).json({ errorType: 'error', message: error.message });
    }

    req.body.userId = tokenDecoded.id;
    console.log('token verified and decoded: ' + tokenDecoded);

    next();
  });
}

module.exports = app;
