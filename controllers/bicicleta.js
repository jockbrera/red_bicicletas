var Bicicleta = require('../models/bicicleta');

module.exports.list = function(req, res) {
    Bicicleta.getAllbicicletas().then(function(bicicletas) {
        res.render('bicicletas/index', { bicicletas: bicicletas });
    });
};

module.exports.create_get = function(req, res) {
    res.render('bicicletas/create');
};

module.exports.create_post = function(req, res) {
    Bicicleta.getAllbicicletas().then(function(bicicletas) {
        var bici = {
                code: bicicletas.length + 1,
                color: req.body.color,
                modelo: req.body.modelo,
                ubicacion: [req.body.lat, req.body.lon]
            };

        // Add new Bici
        Bicicleta.add(bici).then(function() {
            // Redirect to List page
            res.redirect('/bicicletas');
        }).catch(function(error) {
            // throw the error to let the most outside catch to handle it
            throw Error(error);
        });
    }).catch(function(error) {
        // log the error
        console.log(error);

        res.status(500).send({
            message: 'Error creando bicicleta'
        });
    });
};

module.exports.update_get = function(req, res) {
    Bicicleta.findById(req.params.id).then(function(bici) {
        res.render('bicicletas/update', { bici: bici });
    });
};

module.exports.update_post = function(req, res) {
    var bici = {
            code: req.params.id,
            color: req.body.color,
            modelo: req.body.modelo,
            ubicacion: [req.body.lat, req.body.lon]
        };
    
    // Update bici
    Bicicleta.update(bici).then(function() {
        // Redirect to List page
        res.redirect('/bicicletas');
    });
};

module.exports.delete_post = function(req, res) {
    // Delete Bici
    Bicicleta.deleteById(req.params.id).then(function() {
        // Redirect to List page
        res.redirect('/bicicletas');
    });
};
