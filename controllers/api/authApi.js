const Usuario = require('../../models/usuario');
const jwt = require('jsonwebtoken');
const bcrypt = require('bcrypt');

module.exports = {
    authenticate: function(req, res, next) {
        Usuario.findOne({ email: req.body.email }, function(error, usuario) {
            var token;

            if (error) {
                console.log(error);
                return next(error);
            }

            if (!usuario) {
                return res.status(401).json({
                    errorType: 'error',
                    message: 'Email inválido'
                });
            }

            if (!bcrypt.compareSync(req.body.password, usuario.password)) {
                return res.status(401).json({
                    errorType: 'error',
                    message: 'Password inválido'
                });
            }

            token = jwt.sign({ id: usuario._id }, req.app.get('jwtsecretkey'), { expiresIn: '1h'});

            res.status(200).json({
                usuario: usuario,
                token: token
            });
        });
    },
    forgotpassword: function(req, res, next) {
        Usuario.findOne({ email: req.body.email }, function(error, usuario) {
            if (error) {
                console.log(error);
                return next(error);
            }

            if (!usuario) {
                return res.status(401).json({
                    errorType: 'error',
                    message: 'Email inválido'
                });
            }

            usuario.resetPassword().then(function() {
                res.status(200).json({
                    message: 'Se ha enviado un email a tu casilla para resetear el password'
                });
            }).catch(function(error) {
                console.log(error);
                next(error);
            });
        });
    }
};
