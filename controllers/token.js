const Token = require('../models/token');
const Usuario = require('../models/usuario');

module.exports.confirmation = function(req, res) {
    console.log(req.params.token);
    Token.findByTokenAndPopulate(req.params.token).then(function(token) {
        const data = {
            verificado: true
        };
        
        // Usuario already verified
        if (token.usuario.verificado) {
            // Redirect to user's list
            return res.redirect('/usuarios');
        }
        
        // If user not verified, verify it and save it
        Usuario.update(token.usuario._id, data).then(function(response) {
            // Redirect to homepage
            res.redirect('/');
        }).catch(function(error) {
            // log the error
            console.log(error);

            res.status(404).send({
                message: 'Usuario no encontrado'
            });
        });
    }).catch(function(error) {
        // log the error
        console.log(error);

        res.status(404).send({
            message: 'Token no encontrado'
        });
    });
};